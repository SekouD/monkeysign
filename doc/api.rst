API documentation
=================

GnuPG API
---------

.. automodule:: monkeysign.gpg
   :members:
   :undoc-members:

CLI Interface
-------------

.. automodule:: monkeysign.cli
   :members:
   :undoc-members:

GTK Interface
-------------

.. May fail if the GTK module isn't available, oh well.

.. automodule:: monkeysign.gtkui
   :members:
   :undoc-members:
