UI mockups and design
=====================

We are planning significant changes to the graphical user interface of
Monkeysign in the 3.x branch.

Signing interface
-----------------

 .. image:: sign-ui-mockup.png

Key sharing interface
---------------------

 .. image:: share-ui-mockup.png
