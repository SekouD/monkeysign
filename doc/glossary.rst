.. _glossary:

Terminology
===========

In this documentation, the following definitions are used:

.. glossary::

   QR-code
   QRcode
   QR code
   Bar code
   Barcode
        A "QR code" (abbreviation of Quick Response Code) is sort of
        bar code, an optical label that is designed to be
        machine-readable. A QR code is faster to read by computers
        and contains more information than than regular bar codes,
        which is why it is used in Monkeysign to communicate
        fingerprints. See :wikipedia:`QR code` for more information.

   MTA
   Message Transfer Agent
        A computer program designed to transfer emails between
        different machines, usually running on servers. See
        :wikipedia:`Message Transfer Agent` for more
        information.

   MUA
   Mail User Agent
        A computer program used to read, compose and send email,
        normally ran on user computers. See :wikipedia:`Mail User
        Agent` for more information.

We also try to adhere to the `Modern PGP <http://modernpgp.org/>`_
`terminology <https://github.com/ModernPGP/terminology>`_ when
possible.
