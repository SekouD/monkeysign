=========
 Support
=========

If you have problems or question using Monkeysign, there are several
options at your disposal:

* Try to troubleshoot the issue yourself
* Write to the mailing list
* Chat on IRC
* File bug reports

We of course welcome other contributions like documentation,
translations and patches, see the :doc:`contributing` guide for more
information on how to contribute to the project.

Troubleshooting
---------------

The basic way to troubleshoot Monkeysign is to run the same command as
you did when you had an error with the ``--verbose`` or, if that
doesn't yield satisfactory results, with the ``--debug`` output.

.. note:: The debug output outputs a lot of information, as it shows
          the OpenPGP key material as it is exchanged with GnuPG. It
          may be confusing for new users.

If you suspect there is a bug in Monkeysign specific to your
environment, you can also try to see if it is reproducible within the
test suite with ``monkeysign --test``. From there, you can either file
a bug report or try to fix the issue yourself, see the
:doc:`contributing` section for more information.

Otherwise, see below for more options to get support.

Mailing list
------------

Anyone can write to the mailing list at
monkeysphere@lists.riseup.net. You can `browse the archives
<https://lists.riseup.net/www/arc/monkeysphere>`_ before posting to
see if your question has already been answered. Thanks to `Riseup.net
<https://riseup.net/>`_ for graciously hosting our mailing list.

.. tip:: We encourage you to `donate to Riseup
         <https://riseup.net/en/donate>`_ to support the Monkeysign
         project, as we use several parts of their infrastructure to
         develop Monkeysign.

Note that the mailing list is for the larger `Monkeysphere project
<http://web.monkeysphere.info/>`_ so if you subscribe, you should
expect discussions to go beyond only Monkeysign. Furthermore, when you
write to the mailing list, you should explicitly mention that you are
talking about Monkeysign.

Chat
----

We are often present in realtime in the ``#monkeysphere`` channel of
the `OFTC network <https://www.oftc.net/>`_. You can join the channel
using `this link <ircs://irc.oftc.net/monkeysphere>`_ or `this web
interface
<http://webchat.oftc.net/?nick=monkey.&channels=monkeysphere&prompt=1>`_.

.. raw:: html

         <iframe src="http://webchat.oftc.net/?nick=monkey.&channels=monkeysphere&prompt=1" width="647" height="400"></iframe>

Bug reports
-----------

We want you to report bugs you find in Monkeysign. It's an important
part of contributing to a project, and all bug reports will be read and
replied to politely and professionally.

We are using `0xACAB.org's Gitlab instance
<https://0xacab.org/monkeysphere/monkeysign/issues>`__ to manage
issues, and this is where bug reports should be sent. Some issues are
also documented by Debian users directly.

.. tip:: A few tips on how to make good bug reports:

         * Before you report a new bug, review the existing issues in
           the `online issue tracker`_ and the `Debian BTS for
           Monkeysign`_ to make sure the bug has not already been
           reported elsewhere.

         * The first aim of a bug report is to tell the developers
           exactly how to reproduce the failure, so try to reproduce
           the issue yourself and describe how you did that.

         * If that is not possible, just try to describe what went wrong in
           detail. Write down the error messages, especially if they
           have numbers.

         * Take the necessary time to write clearly and precisely. Say
           what you mean, and make sure it cannot be misinterpreted.

         * Include the output of ``monkeysign --test``, ``monkeysign
           --version`` and ``monkeysign --debug`` in your bug
           reports. See the :doc:`issue template <issue_template>` for
           more details about what to include in bug reports.

         If you wish to read more about issues regarding communication
         in bug reports, you can read `How to Report Bugs
         Effectively`_ which takes about 30 minutes.

.. _online issue tracker: https://0xacab.org/monkeysphere/monkeysign/issues
.. _How to Report Bugs Effectively: http://www.chiark.greenend.org.uk/~sgtatham/bugs.html
         
.. warning:: The output of the ``--debug`` shows public key material
             used by Monkeysign. Special efforts have been made so
             that private key material is never displayed (or in fact
             accessed directly or copied) but you may want to avoid
             publicly disclosing which keys you are signing because
             that can reveal your social graph. If you are confident
             the signed user will publish the results on the public
             keyservers, this is not much of a concern. But otherwise,
             you should leave that decision to that user. This is
             particularly relevant if you do *not* want to publicly
             certify this (e.g. if you are using the ``--local``
             flag). Do review the output before sending it in bug
             reports.

Debian BTS
~~~~~~~~~~

You can also report bugs by email over the `Debian
BTS <http://bugs.debian.org/>`__, even if you are not using Debian. Use
the ``reportbug`` package to report a bug if you run Debian (or Ubuntu),
otherwise send an email to ``submit@bugs.debian.org``, with content like
this:

::

    To: submit@bugs.debian.org
    From: you@example.com
    Subject: fails to frobnicate

    Package: monkeysign
    Version: 1.0
      
    Monkeysign fails to frobnicate.

    I tried to do...

    I was expecting...

    And instead I had this backtrace...

    I am running Arch Linux 2013.07.01, Python 2.7.5-1 under a amd64
    architecture.

See also the `complete
instructions <http://www.debian.org/Bugs/Reporting>`__ for more
information on how to use the Debian bugtracker. You can also browse the
existing bug reports in the `Debian BTS for
Monkeysign <http://bugs.debian.org/monkeysign>`_ there.
